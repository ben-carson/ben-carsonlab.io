# bencarson.net

My personal website, running on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).  

Nothing fancy at this point, using the out-of-the-box GitLab Pages [plain html project](https://gitlab.com/pages/plain-html) workflow. I may complicate it up at some point in the future, but right now, I'm more focused on redesigning the UI and putting up some decent content.

Using [Bulma](https://bulma.io/) for the UI right now because it's good-looking and simple to incorporate.